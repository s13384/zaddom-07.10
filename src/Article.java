
public class Article {

	private float price;
	private String name;
	private float discount; // in %
	
	Article ()
	{
		discount=0;
	}
	Article(float price, String name)
	{
		this();
		this.price=price;
		this.name=name;
	}
	Article(float price, String name, float discount)
	{
		this(price, name);
		this.discount=discount;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getDiscount() {
		return discount;
	}
	public void setDiscount(float discount) {
		this.discount = discount;
	}
	
}
