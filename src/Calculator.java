import java.util.ArrayList;


public class Calculator implements ICalculator {

	private float fullPrice;
	private boolean containsPromoArticle=false;
	@Override
	public void addToBasket(ArrayList<Article> articles, Article article, ArrayList<Article> promotionList) {
		//adding to basket
		articles.add(article);
		//changing price of all items in basket
		if (promotionList.size()>0 && articles.size()>0)
		{
			int i;
			for (i=0;i<=articles.size();i++)
			{
				if (articles.contains(promotionList.get(i)))
				{
					containsPromoArticle=true;
					addFullPrice(article.getPrice()-article.getPrice()*article.getDiscount());
				}
			}
		}		
		else
		{
			addFullPrice(article.getPrice()-article.getPrice()*article.getDiscount());
		}
		
	}
	public float getFullPrice() {
		return fullPrice;
	}
	public void setFullPrice(float fullPrice) {
		this.fullPrice = fullPrice;
	}
	public void addFullPrice(float adding) {
		this.fullPrice += adding;
	}
	public float getBill(Client client)
	{
		float tmpFullprice=fullPrice;
		
		tmpFullprice=tmpFullprice*client.getDiscount();
		if (containsPromoArticle)
		{
			return tmpFullprice*9/10;
		}
		else
		{
			return tmpFullprice;
		}
	}

}
